package com.example.dailysmart;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.inputmethodservice.KeyboardView;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

import org.w3c.dom.Text;

import java.util.Random;

public class NumeroJuego extends AppCompatActivity {
    //Counters
    CountDownTimer countDownTimer3, countDownTimer5, countDownTimer7, countDownTimer9;

    //Components
    TextView tempoView;
    TextView cifraView;
    TextView aciertosView;
    TextView NivelView;
    Button startbtn, enterbtn;
    EditText cifraPuestaTxt;

    //Variables
    int RandomNum, inputed;
    int lvl = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_numero_juego);

        processUI();
        cifraPuestaTxt.setEnabled(false);
        enterbtn.setEnabled(false);
        tempoView.setVisibility(View.INVISIBLE);
        startbtn.setEnabled(true);

        //Contador para inicializar automáticamente el siguiente nivel en caso de acertar
        countDownTimer3 = new CountDownTimer(4000, 1000) {
            @Override
            public void onTick(long millisUntilFinished)
            {
                tempoView.setVisibility(View.VISIBLE);
                tempoView.setText(millisUntilFinished/1000 + "seg");
            }

            @Override
            public void onFinish()
            {
                startbtn.performClick();
            }
        };

        //Contador para memorizar el número dentro de cada nivel
        countDownTimer5 = new CountDownTimer(6000, 1000) {
            @Override
            public void onTick(long millisUntilFinished)
            {
                startbtn.setVisibility(View.INVISIBLE);
                tempoView.setVisibility(View.VISIBLE);
                tempoView.setText(millisUntilFinished/1000 + "seg");
            }

            @Override
            public void onFinish() {
                cifraView.setText("??????");
                tempoView.setVisibility(View.INVISIBLE);
                enterbtn.setEnabled(true);
                cifraPuestaTxt.setEnabled(true);
                startbtn.setEnabled(false);

                //Activar el botón para introducir texto
                cifraPuestaTxt.requestFocus();
                InputMethodManager GuessNum = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                GuessNum.showSoftInput(cifraPuestaTxt, InputMethodManager.SHOW_IMPLICIT);

                //Introduce el número al presionar Enter en el teclado
                cifraPuestaTxt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_GO)
                        {
                            enterbtn.performClick();
                        }
                        return false;
                    }
                });
            }
        };

        //Contador para finalizar el módulo en caso de terminar los 10 niveles
        countDownTimer7 = new CountDownTimer(6000, 1000)
        {
            @Override
            public void onTick(long millisUntilFinished)
            {
                tempoView.setVisibility(View.VISIBLE);
                tempoView.setText(millisUntilFinished/1000 + "seg");
                cifraView.setText("FIN");
                NivelView.setText("Completaste el nivel 10");
            }

            @Override
            public void onFinish()
            {
                finish();
            }
        };

        startbtn.setOnClickListener(new View.OnClickListener() { //Al hacer clic en Comenzar
            @Override
            public void onClick(View v) {
                NivelView.setVisibility(View.VISIBLE);
                //Variables para el rango de valores aleatorios
                int RangoMax, RangoMin, Rango;

                //Inicia cada nivel
                if(lvl == 1)
                {
                    NivelView.setText("Nivel: " + String.valueOf(lvl));
                    RangoMin = 1;
                    RangoMax = 10;
                    Rango = (RangoMax - RangoMin) + 1;
                    RandomNum = (int) (Math.random() * Rango);
                    cifraView.setText(String.valueOf(RandomNum));
                    countDownTimer5.start();
                }
                else if(lvl == 2)
                {
                    RangoMin = 10;
                    RangoMax = 100;
                    Rango = (RangoMax - RangoMin) + 9;
                    RandomNum = (int) (Math.random() * Rango);
                    if (RandomNum < 10)
                    {
                        RandomNum = RandomNum + 10;
                        cifraView.setText(String.valueOf(RandomNum));
                    }
                    else
                    {
                        cifraView.setText(String.valueOf(RandomNum));
                    }
                    countDownTimer5.start();
                }
                else if(lvl == 3)
                {
                    RangoMin = 100;
                    RangoMax = 1000;
                    Rango = (RangoMax - RangoMin) + 99;
                    RandomNum = (int) (Math.random() * Rango);
                    if (RandomNum < 100)
                    {
                        RandomNum = RandomNum + 100;
                        cifraView.setText(String.valueOf(RandomNum));
                    }
                    else
                    {
                        cifraView.setText(String.valueOf(RandomNum));
                    }
                    countDownTimer5.start();
                }
                else if(lvl == 4)
                {
                    RangoMin = 1000;
                    RangoMax = 10000;
                    Rango = (RangoMax - RangoMin) + 999;
                    RandomNum = (int) (Math.random() * Rango);
                    if (RandomNum < 1000)
                    {
                        RandomNum = RandomNum + 1000;
                        cifraView.setText(String.valueOf(RandomNum));
                    }
                    else
                    {
                        cifraView.setText(String.valueOf(RandomNum));
                    }
                    countDownTimer5.start();
                }
                else if(lvl == 5)
                {
                    RangoMin = 10000;
                    RangoMax = 100000;
                    Rango = (RangoMax - RangoMin) + 9999;
                    RandomNum = (int) (Math.random() * Rango);
                    if (RandomNum < 10000)
                    {
                        RandomNum = RandomNum + 10000;
                        cifraView.setText(String.valueOf(RandomNum));
                    }
                    else
                    {
                        cifraView.setText(String.valueOf(RandomNum));
                    }
                    countDownTimer5.start();
                }
                else if (lvl == 6)
                {
                    RangoMin = 100000;
                    RangoMax = 1000000;
                    Rango = (RangoMax - RangoMin) + 99999;
                    RandomNum = (int) (Math.random() * Rango);
                    if (RandomNum < 100000)
                    {
                        RandomNum = RandomNum + 100000;
                        cifraView.setText(String.valueOf(RandomNum));
                    }
                    else
                    {
                        cifraView.setText(String.valueOf(RandomNum));
                    }
                    countDownTimer5.start();
                }
                else if (lvl == 7)
                {
                    RangoMin = 1000000;
                    RangoMax = 10000000;
                    Rango = (RangoMax - RangoMin) + 999999;
                    RandomNum = (int) (Math.random() * Rango);
                    if (RandomNum < 1000000)
                    {
                        RandomNum = RandomNum + 1000000;
                        cifraView.setText(String.valueOf(RandomNum));
                    }
                    else
                    {
                        cifraView.setText(String.valueOf(RandomNum));
                    }
                    countDownTimer5.start();
                }
                else if (lvl == 8)
                {
                    RangoMin = 10000000;
                    RangoMax = 100000000;
                    Rango = (RangoMax - RangoMin) + 9999999;
                    RandomNum = (int) (Math.random() * Rango);
                    if (RandomNum < 10000000)
                    {
                        RandomNum = RandomNum + 10000000;
                        cifraView.setText(String.valueOf(RandomNum));
                    }
                    else
                    {
                        cifraView.setText(String.valueOf(RandomNum));
                    }
                    countDownTimer5.start();
                }
                else if (lvl == 9)
                {
                    RangoMin = 100000000;
                    RangoMax = 1000000000;
                    Rango = (RangoMax - RangoMin) + 99999999;
                    RandomNum = (int) (Math.random() * Rango);
                    if (RandomNum < 100000000)
                    {
                        RandomNum = RandomNum + 100000000;
                        cifraView.setText(String.valueOf(RandomNum));
                    }
                    else
                    {
                        cifraView.setText(String.valueOf(RandomNum));
                    }
                    countDownTimer5.start();
                }
                else if (lvl == 10) //lvl 10 se sale del rango INT
                {
                    RangoMin = 1000000000;
                    RangoMax = Integer.MAX_VALUE;
                    Rango = (RangoMax - RangoMin) + 999999999;
                    RandomNum = (int) (Math.random() * Rango);
                    if (RandomNum < 1000000000)
                    {
                        RandomNum = RandomNum + 1000000000;
                        cifraView.setText(String.valueOf(RandomNum));
                    }
                    else
                    {
                        cifraView.setText(String.valueOf(RandomNum));
                    }
                    countDownTimer5.start();
                }
            }
        });

        enterbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkNumber();
            }
        });
    }

    public void checkNumber()
    {
        inputed = 0;
        try
        {
            inputed = Integer.parseInt(cifraPuestaTxt.getText().toString());
        }
        catch (NumberFormatException exp)
        {
            //?
        }
        if (RandomNum == inputed) //Si se acertó el número
        {
            if (lvl == 10) //Nivel 10 es el final
            {
                countDownTimer7.start();
            }
            else
            {
                lvl = lvl + 1;
                NivelView.setText("Nivel: " + String.valueOf(lvl));
                Log.d("OK ME", "CORRECTO!" + RandomNum);
                RandomNum = 0;
                inputed = 0;
                startbtn.setEnabled(true);
                enterbtn.setEnabled(false);
                cifraPuestaTxt.setText("");
                cifraView.setText("CORRECTO" );
                cifraPuestaTxt.setEnabled(false);
                countDownTimer3.start(); //Inicializa el siguiente nivel
            }
        }
        else //Si falla el número
        {
            NivelView.setText("Llegaste al nivel: " + String.valueOf(lvl));
            lvl = 1;
            Log.d("OK ME", "INCORRECTO!" + RandomNum);
            startbtn.setEnabled(true);
            enterbtn.setEnabled(false);
            cifraPuestaTxt.setText("");
            cifraView.setText("INCORRECTO" );
            cifraPuestaTxt.setEnabled(false);
            startbtn.setVisibility(View.VISIBLE);
        }
    }

    public void processUI()
    {
        //Buttons
        startbtn = (Button) findViewById(R.id.comenzarbtn);
        enterbtn = (Button) findViewById(R.id.atinarbtn);
        //TextView
        tempoView = (TextView) findViewById(R.id.tempo); //EL TIEMPO
        cifraView = (TextView) findViewById(R.id.numeroAparecer); //APARECE EL NUMERO
        //EditText
        NivelView = (TextView) findViewById(R.id.lblContadorNivel); //Contador de nivel
        cifraPuestaTxt = (EditText) findViewById(R.id.respuesta); //LEE EL NUMERO
    }


}
