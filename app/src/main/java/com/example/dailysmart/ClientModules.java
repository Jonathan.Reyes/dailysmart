package com.example.dailysmart;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ClientModules extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_modules);

        Button ReactionIniciar = findViewById(R.id.Reactionbtn);
        Button MemoramaIniciar = findViewById(R.id.Memoramabtn);
        Button NumerosIniciar = findViewById(R.id.Numbersbtn);
        Button VisualIniciar = findViewById(R.id.btn_Mem_Visual);

        MemoramaIniciar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent juego2Intent = new Intent(getApplicationContext(), MemoramaJuego.class);
                startActivity(juego2Intent);
            }
        });

        ReactionIniciar.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                Intent juego1Intent = new Intent(getApplicationContext(), ReactionJuego.class);
                startActivity(juego1Intent);
            }
        });

        NumerosIniciar.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                Intent juego3Intent = new Intent(getApplicationContext(), NumeroJuego.class);
                startActivity(juego3Intent);
            }
        });

        VisualIniciar.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                Intent juego4Intent = new Intent(getApplicationContext(), MemoriaVisual.class);
                startActivity(juego4Intent);
            }
        });
    }
}
