package com.example.dailysmart;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.media.MediaPlayer;
import android.net.IpSecManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.Timer;

public class MemoriaVisual extends AppCompatActivity {

    int Nivel=1;//nivel del juego
    int Tamano_M;//tamano de la matriz
    int Objetivos;//cantidad de cuadros que se tendra que recordar

    int min = 0;
    int puntos=0;
    int max = 9;
    int vidas=3;
    Random random;
    List<Integer> CSeleccionados = new ArrayList<>();
    List<Integer> CObjetivos = new ArrayList<>();
    List<ImageView> ListaCuadros = new ArrayList<>();
    Button btnVerificar;
    Button btnIniciar;
    TextView txtNivel;
    LinearLayout vertical1;
    CardView cv;
    LinearLayout fila;
    CountDownTimer TimerAnimacion;
    TextView vidast;
    MediaPlayer incorrecto ;
    MediaPlayer correcto ;
    public static Handler handler = new Handler(); //Utilizaremos un handler para hacer un Runnable

    /////////////////Empieza onCreate
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_memoria_visual);
        //Asignacion de Codigo a la interfaz
        btnVerificar = findViewById(R.id.btn_Verificar);
        btnIniciar = findViewById(R.id.btn_start);
        txtNivel = findViewById(R.id.txtNivel);
        vertical1 = findViewById(R.id.vertical1);
        vidast= findViewById(R.id.txtVidas);

        btnIniciar.bringToFront();
        //Definicion de funciones al accionarse el evento OnClickListener()
        btnIniciar.setOnClickListener(v -> iniciar());
        btnVerificar.setOnClickListener(v -> VerificarAciertos());
        incorrecto = MediaPlayer.create(this,R.raw.incorrecto);
        correcto = MediaPlayer.create(this,R.raw.correcto);
    }
    //Termina onCreate

    /////////////////////funciones y metodos///////////////
    private void iniciar(){
        OcultarIniciar();

        LimpiarCuadros();
        Crear_Filas();
        DefinicionObjetivos();
        mostrarObjetivos();
        txtNivel.setText(Integer.toString(Nivel));
    }

    private void LimpiarCuadros(){
        vertical1.removeViewInLayout(vertical1);
        ListaCuadros.clear();
        CSeleccionados.clear();
        CObjetivos.clear();
    }
    private void Set_Tamano()//Metodo que determina el tamano de la matriz dependiendo del nivel
    {
        if (Nivel<3)
        Tamano_M=3;
        else if (Nivel < 6)
            Tamano_M=4;
        else if (Nivel < 9)
            Tamano_M=5;
        else Tamano_M=6;
    }

    private void mostrarObjetivos(){

        for (ImageView o :
                ListaCuadros){
            o.setClickable(false);
            vertical1.setClickable(false);
            btnIniciar.setClickable(false);
            btnVerificar.setClickable(false);
            for (int seleccionados :
                    CObjetivos) {
                if (ListaCuadros.indexOf(o)==seleccionados){
                    TimerAnimacion = new CountDownTimer(900,800){
                        @Override
                        public void onTick(long millisUntilFinished) {
                            o.callOnClick();
                        }
                        @Override
                        public void onFinish() {
                            for (ImageView w :
                                    ListaCuadros) {
                                w.setClickable(true);
                            }
                           // vertical1.setEnabled(true);
                            btnIniciar.setClickable(true);
                            btnVerificar.setClickable(true);
                            this.cancel();
                        }
                    }.start();
                }
            }
        }
    }

    private void DefinicionObjetivos(){
        Objetivos = Nivel + 2;
        random = new Random();
        int temp;
        max=Tamano_M*Tamano_M;//Numero maximo del random, que seria la cantidad de cubos

        for (int i = 0; i < Objetivos; i++) {  //seleccion de los cuadros aleatoriamente
            temp=random.nextInt((Tamano_M*Tamano_M) - min ) + min;
            if (!CObjetivos.contains(temp))//Verifica que no sea repetido el numero
            {
                CObjetivos.add(temp);
            }
            else{
                i--;
            }
        }
    }

    private LinearLayout.LayoutParams DefinirMargenes(LinearLayout.LayoutParams params){
        params.weight = 1.0f;

        if (Nivel<3) {
            params.leftMargin = 50;
            params.rightMargin=50;
            params.topMargin=35;
            params.bottomMargin=35;
        }
        else if (Nivel < 6)
        {
            params.leftMargin = 50;
            params.rightMargin=50;
            params.bottomMargin = 50;
            params.topMargin=50;
        }
        else if (Nivel < 9)
        {
            params.topMargin=10;
            params.bottomMargin=10;
            params.leftMargin = 10;
            params.rightMargin=10;
        }
        else {
            params.leftMargin = 4;
            params.rightMargin=4;
            params.topMargin=4;
            params.bottomMargin=4;
        }
        return params;
    }

    private void Crear_Filas(){
        Set_Tamano();
        for (int i = 0; i<Tamano_M;i++)
        {
            //LinearLayout vertical1 = findViewById(R.id.vertical1);
            fila = new LinearLayout(this);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams
                            (
                                    LinearLayout.LayoutParams.WRAP_CONTENT,
                                    LinearLayout.LayoutParams.WRAP_CONTENT
                            );

            fila.setLayoutParams(DefinirMargenes(params));
            fila.setOrientation(LinearLayout.HORIZONTAL);
            fila.setId(i+100);

            for (int x = 0; x < Tamano_M; x++ ) { //creacion de cubos
                ImageView imageView = new ImageView(this);
                Drawable drawableOff =getResources().getDrawable(R.drawable.bgpink);
                imageView.setLayoutParams(params);
                imageView.setTag(0);
                Animation anim2=AnimationUtils.loadAnimation(this, R.anim.rotacion);

                imageView.setOnClickListener(v -> {
                    imageView.startAnimation(anim2);

                    if ((Integer)v.getTag() == 0)
                    {
                        imageView.setTag(1);
                        imageView.setColorFilter(Color.rgb(255, 215, 229), PorterDuff.Mode.MULTIPLY);
                    }
                    else {
                        imageView.setTag(0);
                        imageView.clearColorFilter();
                    }
                });
                //imageView.startAnimation(anim);
                imageView.setImageDrawable(drawableOff);
                ListaCuadros.add(imageView);
                fila.addView(imageView);
            }
            vertical1.addView(fila);
        }
    }
    private void VerificarAciertos() {
        OcultarVerificar();
        vertical1.removeAllViews();
        for (ImageView img :
                ListaCuadros) {
            if ((Integer) img.getTag() == 1) {
                CSeleccionados.add(ListaCuadros.indexOf(img));
                img.callOnClick();
            }
        }
        //ordena las listas para poder compararlas
        Collections.sort(CSeleccionados);
        Collections.sort(CObjetivos);
        //si se seleccionan los mismos cuadros el jugador gana
        if (CSeleccionados.containsAll(CObjetivos))  {
            puntos = puntos + 10;
            Nivel=Nivel+1;
            correcto.start();
        } else
            {
            vidas = vidas - 1;
            incorrecto.start();
        }
        vidast.setText(Integer.toString(vidas));
        PerdiooGano();
    }

    private void OcultarIniciar(){
        btnIniciar.setVisibility(View.INVISIBLE);
        btnVerificar.setVisibility(View.VISIBLE);
    }
    private void OcultarVerificar(){
        btnIniciar.setVisibility(View.VISIBLE);
        btnVerificar.setVisibility(View.INVISIBLE);
    }


    private void PerdiooGano() {
        if (vidas == 0) {
            Toast.makeText(this, "Perdiste", Toast.LENGTH_LONG).show();
            Nivel = 1;
            txtNivel.setText(Integer.toString(Nivel));
            vidas = 3;
            vidast.setText(Integer.toString(vidas));

        }
    }
}
